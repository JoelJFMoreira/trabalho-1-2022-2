#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <unistd.h>

#define MAX 100
#define SA struct sockaddr

void func(int sockfd){
    char buff[MAX];
    int n;

    while(1){
        bzero(buff, sizeof(buff));
        printf("Digite a informação: ");
        n = 0;
        
        while((buff[n++] = getchar()) != '\n');
    
        write(sockfd, buff, sizeof(buff));
        bzero(buff, sizeof(buff));
        read(sockfd, buff, sizeof(buff));
        if((strncmp(buff,"exit",4)) == 0){
            printf("Client exit...\n");
            break;
        }
    }
}

int main(int argc, char **argv){
    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;
    int PORT = atoi(argv[1]);
    printf("%d", PORT);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1){
        printf("Criação do socket falhou!\n");
        exit(0);
    }
    else{
        printf("Criação do socket com sucesso!\n");
    }
    
    bzero(&servaddr, sizeof(servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);

    if(connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0){
        printf("Conexão com o server falhou!\n");
        exit(0);
    }
    
    else{
        printf("Conectado ao server!\n");
    }

    func(sockfd);

    close(sockfd);
}