cclient:
	@gcc ./src/RPIClient.c ./src/cJSON.c -o Client.out -lbcm2835 -lpthread

serv:
	@gcc ./src/RPIServer.c ./src/cJSON.c -o Serv.out -lpthread
	@./Serv.out

1client: cclient
	@./Client.out configuracao_sala_01.json

2client: cclient
	@./Client.out configuracao_sala_02.json

clean:
	@rm -f *.out