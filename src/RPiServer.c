#include <stdio.h> 
#include <string.h>   //strlen 
#include <stdlib.h> 
#include <errno.h> 
#include <unistd.h>   //close 
#include <arpa/inet.h>    //close 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <pthread.h>
#include <semaphore.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros 
     
#define TRUE   1 
#define FALSE  0 
#define PORT 10833 //10831 10840

int escolha = -1, sala = 0;
sem_t semaforo, semaforo2;


void msgFunc(int sd, char *buffer);
void communication(int master_socket, int max_clients, int *client_socket, struct sockaddr_in address);
void *menu();
void controleSala();
void checkSalaEnvia(char *msg, int sala, int *client_socket, int sd, int max_clients);

int main(int argc , char *argv[]){  
    int opt = TRUE;  
    int master_socket, client_socket[30],max_clients = 30, valread, i;  
    struct sockaddr_in address;
    pthread_t t1;
     
    //initialise all client_socket[] to 0 so not checked 
    for (i = 0; i < max_clients; i++){  
        client_socket[i] = 0;  
    }  
         
    //create a master socket 
    if((master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0){  
        perror("socket failed");  
        exit(EXIT_FAILURE);  
    }  
     
    //set master socket to allow multiple connections , 
    //this is just a good habit, it will work without this 
    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ){  
        perror("setsockopt");  
        exit(EXIT_FAILURE);  
    }  
     
    //type of socket created 
    address.sin_family = AF_INET;  
    address.sin_addr.s_addr = INADDR_ANY;  
    address.sin_port = htons( PORT );  
         
    //bind the socket to localhost port 8888 
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0){  
        perror("bind failed");  
        exit(EXIT_FAILURE);  
    }

    printf("Listener on port %d \n", PORT);  
         
    //try to specify maximum of 3 pending connections for the master socket 
    if (listen(master_socket, 3) < 0){  
        perror("listen");  
        exit(EXIT_FAILURE);  
    }  
         
    //accept the incoming connection  
    puts("Waiting for connections ...");

    sem_init(&semaforo,0,1);
    sem_init(&semaforo2,0,1);
    pthread_create(&t1, NULL, menu, NULL);

    communication(master_socket, max_clients, client_socket, address);
    pthread_join(t1, NULL);
    return 0;
}

void msgFunc(int sd, char *buffer){
    send(sd,buffer,strlen(buffer),0);
}
     
void communication(int master_socket, int max_clients, int *client_socket, struct sockaddr_in address){
    char *message = "ECHO Daemon v1.0 \r\n";
    char buffer[1025];  //data buffer of 1K 
    int i, max_sd, valread, sd, new_socket, activity, addrlen;
    //set of socket descriptors 
    fd_set readfds;
    addrlen = sizeof(address); 
    
    while(TRUE){
        //clear the socket set 
        FD_ZERO(&readfds);  
        max_sd = master_socket;
     
        //add master socket to set 
        FD_SET(master_socket, &readfds);  
             
        //add child sockets to set 
        for ( i = 0 ; i < max_clients ; i++){  
            //socket descriptor 
            sd = client_socket[i];  
                 
            //if valid socket descriptor then add to read list 
            if(sd > 0)  
                FD_SET( sd , &readfds);  
                 
            //highest file descriptor number, need it for the select function 
            if(sd > max_sd)  
                max_sd = sd;  
        }  
     
        //wait for an activity on one of the sockets , timeout is NULL , 
        //so wait indefinitely 
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);  
       
        if ((activity < 0) && (errno!=EINTR)){  
            printf("select error");  
        }  
             
        //If something happened on the master socket , 
        //then its an incoming connection 
        if (FD_ISSET(master_socket, &readfds)){
            if ((new_socket = accept(master_socket,(struct sockaddr *)&address, (socklen_t*)&addrlen))<0){  
                perror("accept");  
                exit(EXIT_FAILURE);  
            }  
             
            //inform user of socket number - used in send and receive commands 
            printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));  
           
            //send new connection greeting message 
            if( send(new_socket, message, strlen(message), 0) != strlen(message)){  
                perror("send");  
            }  
                 
            puts("Welcome message sent successfully");  
                 
            //add new socket to array of sockets 
            for (i = 0; i < max_clients; i++){
                //if position is empty 
                if( client_socket[i] == 0 ){  
                    client_socket[i] = new_socket;  
                    printf("Adding to list of sockets as %d\n" , i);  
                         
                    break;  
                }  
            }
        }
             
        //else its some IO operation on some other socket
        for (i = 0; i < max_clients; i++){  
            sd = client_socket[i];  
                 
            if (FD_ISSET( sd , &readfds)){  
                //Check if it was for closing , and also read the 
                //incoming message 
                if ((valread = read( sd , buffer, 1024)) == 0){  
                    //Somebody disconnected , get his details and print 
                    getpeername(sd, (struct sockaddr*)&address, (socklen_t*)&addrlen);  
                    
                    printf("Host disconnected , ip %s , port %d \n", inet_ntoa(address.sin_addr), ntohs(address.sin_port));  
                         
                    //Close the socket and mark as 0 in list for reuse 
                    close( sd );  
                    client_socket[i] = 0;
                }  
                     
                //Echo back the message that came in 
                else{  
                    //set the string terminating NULL byte on the end 
                    //of the data read 
                    buffer[valread] = '\0';
                }
            }
            sem_wait(&semaforo2);
            if(sala != -1){
                sem_wait(&semaforo);
                switch (escolha){
                    case 1: //Mudar os estados da lampada 1
                        strcpy(buffer,"!lam1");
                        checkSalaEnvia(buffer, sala, client_socket, sd , max_clients);
                        printf("Foi enviado: %s\n",buffer);
                        escolha = 0;
                    break;
                    case 2: //Mudar os estados da lampada 2
                        strcpy(buffer,"!lam2");
                        checkSalaEnvia(buffer, sala, client_socket, sd , max_clients);
                        printf("Foi enviado: %s\n",buffer);
                        escolha = 0;
                    break;
                    case 3: //Mudar os estados de todas as lampadas
                        strcpy(buffer,"!lam3");
                        checkSalaEnvia(buffer, sala, client_socket, sd , max_clients);
                        printf("Foi enviado: %s\n",buffer);
                        escolha = 0;
                    break;
                    case 4://Projetor
                        strcpy(buffer,"!proj");
                        checkSalaEnvia(buffer, sala, client_socket, sd , max_clients);
                        printf("Foi enviado: %s\n",buffer);
                        escolha = 0;
                    break;
                    case 5://Ar
                        strcpy(buffer,"!arcn");
                        checkSalaEnvia(buffer, sala, client_socket, sd , max_clients);
                        printf("Foi enviado: %s\n",buffer);
                        escolha = 0;
                    break;
                    case 6://Alarme
                        strcpy(buffer,"!alar");
                        checkSalaEnvia(buffer, sala, client_socket, sd , max_clients);
                        printf("Foi enviado: %s\n",buffer);
                        escolha = 0;
                    break;
                    case 7:
			            strcpy(buffer, "!stat");
			            checkSalaEnvia(buffer, sala, client_socket, sd, max_clients);
                        printf("Foi enviado: %s\n",buffer);
                        usleep(5000);
			            bzero(buffer, 1024);
                        valread = read(sd, buffer, 1024);
                        if (buffer[0]=='0'){
                            printf("O sensor da lampada 1 esta desavito\t");
                        } else if (buffer[0]=='1'){
                            printf("O sensor da lampada 1 esta avito\n");
                        }
                        if (buffer[1]=='0'){
                            printf("O sensor da lampada 2 esta desavito\t");
                        } else if (buffer[1]=='1'){
                            printf("O sensor da lampada 2 esta avito\n");
                        }
                        if (buffer[2]=='0'){
                            printf("O sensor da ar esta desavito\t");
                        } else if (buffer[2]=='1'){
                            printf("O sensor da ar esta avito\n");
                        }
                        if (buffer[3]=='0'){
                            printf("O sensor da projetor esta desavito\t");
                        } else if (buffer[3]=='1'){
                            printf("O sensor daprojetor esta avito\n");
                        }
                        if (buffer[4]=='0'){
                            printf("O sensor da sistema de alarme esta desavito\t");
                        } else if (buffer[4]=='1'){
                            printf("O sensor da sistema de alarme avito\n");
                        }
                        if (buffer[5]=='0'){
                            printf("O sensor da sensor de presenca esta desavito\t");
                        } else if (buffer[5]=='1'){
                            printf("O sensor da sensor de presenca esta avito\n");
                        }
                        if (buffer[6]=='0'){
                            printf("O sensor da sensor da fumaca esta desavito\t");
                        } else if (buffer[6]=='1'){
                            printf("O sensor da sensor de fumaca esta avito\n");
                        }
                        if (buffer[7]=='0'){
                            printf("O sensor da sensor da janela esta desavito\t");
                        } else if (buffer[7]=='1'){
                            printf("O sensor da sensor da janela esta avito\n");
                        }
                        if (buffer[8]=='0'){
                            printf("O sensor da lampada sensor da porta esta desavito\t");
                        } else if (buffer[8]=='1'){
                            printf("O sensor da lampada sensor da porta esta avito\n");
                        }
                        if (buffer[9]=='0'){
                            printf("O sensor da lampada buzzer esta desavito\t");
                        } else if (buffer[9]=='1'){
                            printf("O sensor da lampada buzzer esta avito\n");
                        escolha = 0;
                        }
                    break;
                    default:
                        strcpy(buffer, "continue");
                        checkSalaEnvia(buffer, 0, client_socket,sd , max_clients);
                        escolha = 0;
                    break;
                }
                sem_post(&semaforo);
                sem_post(&semaforo2);

            }else{
                sem_wait(&semaforo);
                printf("encerrando tudo\n");
                strcpy(buffer,"exit");
                
                for(i = 0;i <max_clients; i++){
                    sd = client_socket[i];
                    send(sd,buffer, strlen(buffer), 0);
                }
                escolha = -1;
                sem_post(&semaforo);
                sem_post(&semaforo2);
                return;
            }
        }
    } 
}

void *menu(){
    while(1){
        printf("Digite -1 para sair do programa.\n");
        printf("Qual sala voce deseja controlar:\n");
        printf("Digite 0 para controlar todas as salas.\n");
        sem_wait(&semaforo2);
        scanf("%d", &sala);
        sem_post(&semaforo2);
        if(sala == -1)break;
        controleSala();
        usleep(100);
    }
}

void controleSala(){
    printf("Controlando a Sala %d \n", sala);
    printf("Digite o número da opcao que desejar:\n");
    printf("1 - On/Off a lampada 1\t");
    printf("2 - On/Off a lampada 2\n");  
    printf("3 - On/Off todas as lampadas\t");
    printf("4 - On/Off projetor\n");
    printf("5 - On/Off ar-condicionado\t");
    printf("6 - On/Off sistema de alarme\n");
    printf("7 - Status da sala\n");
    printf("Digite -1 para escolher outra sala.\n");
    sem_wait(&semaforo);
    scanf("%d", &escolha);
    sem_post(&semaforo);
}

void checkSalaEnvia(char *msg, int sala, int *client_socket, int sd, int max_clients){
    if(sala != 0){
        if((sd = client_socket[sala-1])==0)
            printf("Sala nao encontrada\n");
        else   
            msgFunc(sd,msg);
        strcpy(msg,"continue");// Mandando o check para todos os clientes nao travarem
        for(int i = 0;i <max_clients; i++){
            sd = client_socket[i];
            if (client_socket[sala-1] != sd);
        }
    }else{ //A sala 0 é o comando geral
        for(int i = 0;i <max_clients; i++){
            sd = client_socket[i];
            msgFunc(sd,msg);
        }
    }
}

