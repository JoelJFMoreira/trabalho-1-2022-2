#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <bcm2835.h>
#include <pthread.h>

//Configuração
//Saidas
#define L_01 RPI_V2_GPIO_P1_37     // BCM 26
#define L_02 RPI_V2_GPIO_P1_35     // BCM 19
#define AC RPI_V2_GPIO_P1_33       // BCM 13
#define PR RPI_V2_GPIO_P1_31       // BCM 06
#define AL_BZ RPI_V2_GPIO_P1_29    // BCM 05

//Entradas
#define SPres 0                    // BCM 11 | gpio0
#define SFum RPI_V2_GPIO_P1_23     // BCM 11
#define SJan RPI_V2_GPIO_P1_21     // BCM 09
#define SPor RPI_V2_GPIO_P1_19     // BCM 10
#define SC_IN RPI_V2_GPIO_P1_15    // BCM 22
#define SC_OUT RPI_V2_GPIO_P1_13   // BCM 27
#define DHT22 RPI_V2_GPIO_P1_12    // BCM 18

//flags 
int alarmeAtivo = 0;
int presenca = 0;


// Inicializacao
void pinsSetup();

// Controle das lampadas
void apagarLampadas();
void acenderLampada();

// Controle Projetor
void ligarProjetor();
void desligarProjetor();

// Controle Ar
void ligarAr();
void desligarAr();

// Controle Alarme
void ligarAlarme();
void desligarAlarme();

// Sistema do Alarme
int ligarSistemaAlarme();
int checkarSensoresAlarme();

void encerrarPrograma(int sinal);

// Contagem de pessoas na sala
int quantidadePessoasSala();

pthread_t sensorsId;
//    pthread_t socksComunicationId;

// Sensoriamento
void* sensoriamento(void *vargs);
void funcDHT22();
short readDataDHT();


int main(int argc, char **argv){   
    pthread_create(&sensorsId, NULL, &sensoriamento, NULL);

    //    pthread_create(&socksComunicationId, NULL, temperatureChecker, NULL);

    if (!bcm2835_init())
        exit(1);

    pinsSetup();

    signal(SIGINT, encerrarPrograma);

    int counter;

    while (1){
        counter = 0;


        delay(1000);
    }
}

void pinsSetup(){

    // Define botão como entrada
    bcm2835_gpio_fsel(SPres, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SFum, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SJan, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SPor, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SC_IN, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SC_OUT, BCM2835_GPIO_FSEL_INPT);
    // Configura entrada do botão como Pull-down
    bcm2835_gpio_set_pud(SPres, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SFum, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SJan, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SPor, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SC_IN, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SC_OUT, BCM2835_GPIO_PUD_DOWN);

    // Configura pinos dos LEDs como saídas
    bcm2835_gpio_fsel(L_01, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(L_02, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(AC, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(PR, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(AL_BZ, BCM2835_GPIO_FSEL_OUTP);
}

//===================================================================================================================
//                                        Controle das lampadas
//===================================================================================================================
void apagarLampadas(){
    bcm2835_gpio_write(L_01, LOW );
    bcm2835_gpio_write(L_02, LOW );
}

void acenderLampada(int qntPessoas) {
    bcm2835_gpio_write(L_01, HIGH );
    bcm2835_gpio_write(L_02, HIGH );
}

//===================================================================================================================
//                                        Controle do Projetor
//===================================================================================================================
void ligarProjetor(){
    bcm2835_gpio_write(PR, HIGH );
}
void desligarProjetor(){
    bcm2835_gpio_write(PR, LOW );
}

//===================================================================================================================
//                                        Controle do Ar-Condicionado
//===================================================================================================================
void ligarAr(){
    bcm2835_gpio_write(AC, HIGH );
}
void desligarAr(){
    bcm2835_gpio_write(AC, LOW );
}

//===================================================================================================================
//                                        Controle do Alarme
//===================================================================================================================
void ligarAlarme(){
    bcm2835_gpio_write(AL_BZ, HIGH );
}
void desligarAlarme(){
    bcm2835_gpio_write(AL_BZ, LOW );
}


void encerrarPrograma(int sinal) {
    apagarLampadas();
    desligarProjetor();
    desligarAr();
    desligarAlarme();
    pthread_join(sensorsId, NULL);
    //    pthread_join(socksComunicationId, NULL);
    bcm2835_close();
    exit(0);
}

void* sensoriamento(void *vargs){
    int contIN=0, contOUT=0,contTemp = 0, qntPessoas = 0, auxIN=0,auxOUT=0, tempLampada=0,ativaAlarme = 0;
    while (1)
    {
        delay(50);
        contTemp++;
        tempLampada++;
        // if(contTemp >=2){
        //     printf("Existem %d pessoas na sala\n", qntPessoas);
        //     contTemp = 0;
        // }
        if(bcm2835_gpio_lev(SC_IN)){
            contIN++;
            if(alarmeAtivo){
                bcm2835_gpio_write(AL_BZ, HIGH );
            }
        }else if(bcm2835_gpio_lev(SC_OUT)){
            contOUT++;
        }else{
            if(contIN >= 2 && contIN <=6){
                auxIN = 1;
            }
            if(auxIN == 1){
                qntPessoas++;
            }
            contIN=0;
            auxIN=0;
            if(contOUT >= 2 && contOUT <=6){
                auxOUT = 1;
            }
            if(auxOUT == 1){
                qntPessoas--;
                auxOUT = 0;
            }
            contOUT=0;
            auxOUT=0;
        }
        if(qntPessoas >= 1){
            acenderLampada(qntPessoas);
            ligarProjetor();
            ligarAr();
            tempLampada=0;
        }else{
            if(tempLampada >= 300){
                apagarLampadas();
                tempLampada=0;
            }
            desligarProjetor();
            desligarAr();
        }

        if (contTemp%20==0){
            
            printf("Existem %d pessoas na sala\n", qntPessoas);
            printf("Sitema de alarme em %d\n", ligarSistemaAlarme());
            ativaAlarme++;
            if (ativaAlarme>=8 && ativaAlarme <=15){
               alarmeAtivo = ligarSistemaAlarme();
               printf("Sistema de alarme ativado, contagem em %d\n", ativaAlarme);
            }else if (ativaAlarme >=15){
                printf("Sistema de alarme desativado, contagem em %d\n", ativaAlarme);
                desligarAlarme();
                ativaAlarme =0;
                alarmeAtivo = 0;
            }
        }
        if (contTemp >= 40){
            funcDHT22();
            contTemp =0;
        }
        
    }

}


int ligarSistemaAlarme(){
    int check=0;
    check = checkarSensoresAlarme();
    if (check == 1){
    //        printf("Sistema de alarme ligado");
        return 1;
    }else if(check == -2){
    //        printf("Sensor da Janela ativado, desative antes de ligar o sistema de alarme.\n");
        return 0;
    }else if(check == -1){
    //        printf("Sensor de Presença ativado, desative antes de ligar o sistema alarme.\n");
        return 0;
    }else{
    //        printf("Falha ao checkar o alarme.\n");
        return 0;
    }
}

int checkarSensoresAlarme(){
    if(!bcm2835_gpio_lev(SPres)){
        if(!bcm2835_gpio_lev(SJan)){
            return 1;
        }else{
            return -2;
        }
    }else{
        return -1;
    }
}
