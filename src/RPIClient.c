#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <bcm2835.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <dirent.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include "cJSON.h"

//Configuração pinos
//Saidas
int L_01, L_02, AC, PR, AL_BZ;
int SPres, SFum, SJan, SPor, SC_IN, SC_OUT, DHT22;

// #define L_01 RPI_V2_GPIO_P1_37     // BCM 26
// #define L_02 RPI_V2_GPIO_P1_35     // BCM 19
// #define AC RPI_V2_GPIO_P1_33       // BCM 13
// #define PR RPI_V2_GPIO_P1_31       // BCM 06
// #define AL_BZ RPI_V2_GPIO_P1_29    // BCM 05

// //Entradas
// #define SPres 0                    // BCM 11 | gpio0
// #define SFum RPI_V2_GPIO_P1_23     // BCM 11
// #define SJan RPI_V2_GPIO_P1_21     // BCM 09
// #define SPor RPI_V2_GPIO_P1_19     // BCM 10
// #define SC_IN RPI_V2_GPIO_P1_15    // BCM 22
// #define SC_OUT RPI_V2_GPIO_P1_13   // BCM 27
// #define DHT22 RPI_V2_GPIO_P1_12    // BCM 18

#define QNT_SENSORES 10

#define MAXTIMINGS 100
int bits[250], data[100];
int bitidx = 0;

//Porta do sockets 
#define PORT 10833                 //10831 10840

//flags 
int fSFum = 0;
int fLam1 = 0;
int fLam2 = 0;
int fProjetor = 0;
int fAr = 0;
int fAlarme = 0;
int fBz = 0;
int client_fd = 0;


int qntPessoas = 0;
//Dados enviados ao servidores
int dados[QNT_SENSORES];
char charDados[QNT_SENSORES];

sem_t semLampada;

// Inicializacao
void setPins(int pin,char alltags[][38],char tag[]);
void pinsSetup();

int socketSetup();
// Controle das lampadas
void apagarLampadas(int numLamp);
void acenderLampada(int numLamp);

// Controle Projetor
void ligarProjetor();
void desligarProjetor();

// Controle Ar
void ligarAr();
void desligarAr();

// Controle Alarme
void ligarAlarme();
void desligarAlarme();

// Sistema do Alarme
int ligarSistemaAlarme();
int checkarSensoresAlarme();

void encerrarPrograma();

// Contagem de pessoas na sala
//int quantidadePessoasSala();

pthread_t sensorsId;

// Sensoriamento
void* sensoriamento(void *vargs);


int main(int argc, char **argv){
    int i=0;
    char archive[1000];
    FILE *json;
    cJSON *ip_servidor_central = NULL;
    cJSON *porta_servidor_central = NULL;
    cJSON *ip_servidor_distribuido = NULL;
    cJSON *porta_servidor_distribuido = NULL;
    cJSON *file_parser = NULL;
    cJSON *nome = NULL;
    cJSON* dispositivo = NULL;
    cJSON *type = NULL;
    cJSON *tag = NULL;
    cJSON *pino = NULL;
    cJSON *outputs = NULL;
    cJSON *inputs = NULL;
    cJSON *sensortemp = NULL;
    char filebff[5200];
    char alltags[13][38] = {"Lâmpada 01", "Lâmpada 02", "Projetor Multimidia", "Ar-Condicionado (1º Andar)", "Sirene do Alarme", "Sensor de Presença", "Sensor de Fumaça", "Sensor de Janela", "Sensor de Porta", "Sensor de Contagem de Pessoas Entrada", "Sensor de Contagem de Pessoas Saída"};

    strcpy(archive,argv[1]);
    json = fopen(archive, "r");

    fread(filebff,5048,1,json);
    file_parser = cJSON_Parse(filebff);

    if(file_parser == NULL){
        printf("Parse falhou...\n");
        exit(-1);
    }

    ip_servidor_central = cJSON_GetObjectItem(file_parser,"ip_servidor_central");
    porta_servidor_central = cJSON_GetObjectItem(file_parser, "porta_servidor_central");
    ip_servidor_distribuido = cJSON_GetObjectItem(file_parser,"ip_servidor_distribuido");
    porta_servidor_distribuido = cJSON_GetObjectItem(file_parser, "porta_servidor_distribuido");
    nome = cJSON_GetObjectItem(file_parser, "nome");
    outputs = cJSON_GetObjectItem(file_parser, "outputs");
    for(i = 0; i < cJSON_GetArraySize(outputs);i++){
        dispositivo = cJSON_GetArrayItem(outputs, i);
        type = cJSON_GetObjectItem(dispositivo, "type");
        tag = cJSON_GetObjectItem(dispositivo, "tag");
        pino = cJSON_GetObjectItem(dispositivo, "gpio");
        setPins(pino->valueint, alltags, tag->valuestring);
    }

    inputs = cJSON_GetObjectItem(file_parser, "inputs");
    for(i = 0; i < cJSON_GetArraySize(inputs);i++){
        dispositivo = cJSON_GetArrayItem(inputs, i);
        type = cJSON_GetObjectItem(dispositivo, "type");
        tag = cJSON_GetObjectItem(dispositivo, "tag");
        pino = cJSON_GetObjectItem(dispositivo, "gpio");
        setPins(pino->valueint, alltags, tag->valuestring);
    }

    sensortemp = cJSON_GetObjectItem(file_parser, "sensor_temperatura");

        dispositivo = cJSON_GetArrayItem(sensortemp, 0);
        type = cJSON_GetObjectItem(dispositivo, "type");
        tag = cJSON_GetObjectItem(dispositivo, "tag");
        pino = cJSON_GetObjectItem(dispositivo, "gpio");
        DHT22 = pino->valueint;

    fclose(json);

    int sock =0;
    int valread;
    int alarmError;
    char buffer[1024] = { 0 };
    char msg[5];

    sem_init(&semLampada,0,1);
    pthread_create(&sensorsId, NULL, &sensoriamento, NULL);

    if (!bcm2835_init())
        exit(1);

    pinsSetup();
    sock = socketSetup();
    //signal(SIGINT, encerrarPrograma);

    int counter;

    while (1){

        bzero(buffer, 1024);
        valread = read(sock, buffer, 1024);
        
        if(strncmp(buffer, "exit", 5) == 0){
            encerrarPrograma();
            break;
        }

        else{
            if(strncmp(buffer,"!lam3", 5) == 0){
                if(fLam1==1 && fLam2==1){
                    apagarLampadas(3);
                    printf("Lampadas apagadas\n");
                }
                else{
                    acenderLampada(3);
                    printf("Lampadas ligadas\n");
                }
                printf("Tem coisa aqui?\n =>%s\n", buffer);
            }else if(strncmp(buffer,"!lam1", 5) == 0){
                if(fLam1==1){
                    apagarLampadas(1);
                    printf("Lampada 1 apagada\n");
                }
                else{
                    acenderLampada(1);
                    printf("Lampada 1 ligada\n");
                }
                printf("Tem coisa aqui?\n =>%s\n", buffer);
            }else if(strncmp(buffer,"!lam2", 5) == 0){
                if(fLam2==1){
                    apagarLampadas(2);
                    printf("Lampada 2 apagada\n");
                }else{
                    acenderLampada(2);
                    printf("Lampada 2 ligada\n");
                }
                printf("Tem coisa aqui?\n =>%s\n", buffer);
            }else if(strncmp(buffer,"!proj", 5) == 0){
                if(fProjetor==1){
                    desligarProjetor();
                    printf("Projetor desligado\n");
                }else{
                    ligarProjetor();
                    printf("Projetor ligado\n");
                
                }
                printf("Tem coisa aqui?\n =>%s\n", buffer);
            }else if(strncmp(buffer,"!arcn", 5) == 0){
                if(fAr==1){
                    desligarAr();
                    printf("Ar-Condicionado desligado\n");
                } 
                else{
                    ligarAr();
                    printf("Ar-Condicionado ligado\n");
                }
                printf("Tem coisa aqui?\n =>%s\n", buffer);
            }else if(strncmp(buffer,"!alar", 5) == 0){
                if(fAlarme==1){
                    desligarAlarme();
                    fAlarme=0;
                    printf("Sistema de alarme desligado\n");
                }else{
                    alarmError = checkarSensoresAlarme();
                    if (alarmError==1){
                        printf("Sistema de alarme ligado\n");
                        ligarSistemaAlarme();
                    }else if (alarmError == -2){
                        printf("Sensor da Janela ativado, desative antes de ligar o sistema de alarme.\n");
                    }else if(alarmError == -1){
                       printf("Sensor de Presença ativado, desative antes de ligar o sistema alarme.\n");
                    }
                }
                printf("Tem coisa aqui?\n =>%s\n", buffer);
            }else if (strncmp(buffer, "!stat", 5) == 0){
                dados[0]=fLam1;
                dados[1]=fLam2;
                dados[2]=fAr;
                dados[3]=fProjetor;
                dados[4]=fAlarme;
                dados[5]=bcm2835_gpio_lev(SPres);
                dados[6]=bcm2835_gpio_lev(SFum);
                dados[7]=bcm2835_gpio_lev(SJan);
                dados[8]=bcm2835_gpio_lev(SPor);
                dados[9]=fBz;
                //dados[9]=qntPessoas;
                for(int i=0; i<QNT_SENSORES; i++){
                    charDados[i]=dados[i]+'0';
                }
                strncpy(buffer,charDados,QNT_SENSORES);
                send(sock, buffer, strlen(buffer), 0);
                printf("Tem coisa aqui?\n =>%s\n", buffer);
		
            }
            
            strcpy(buffer,"continue");
            send(sock, buffer, strlen(buffer), 0); 
        }
    }

    // closing the connected socket
    encerrarPrograma();
    close(client_fd);
}

int socketSetup(){
    int sock = 0;
    struct sockaddr_in serv_addr;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
 
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
 
    // Convert IPv4 and IPv6 addresses from text to binary
    // form
    if (inet_pton(AF_INET, "192.168.1.129", &serv_addr.sin_addr)<= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    client_fd = connect(sock, (struct sockaddr*)&serv_addr,sizeof(serv_addr));

    if (client_fd < 0){
        perror("Connection Failed");
        return -1;
    }

    return sock;
}

//===================================================================================================================
//                                        Configuracao de pinos
//===================================================================================================================
void setPins(int pin,char alltags[][38],char tag[]){
    int value, i;
    for(i=0;i<12;i++){
        if(strcmp(alltags[i],tag) == 0){
            value = i;
            break;
        }
    }
    printf("Tag %s e o pino %d\n", tag, pin);
    if (value == 0){ 
        L_01 = pin;
    }else if (value == 1){
        L_02 = pin;
    }else if (value == 2){
        PR = pin;
    }else if (value == 3){
        AC = pin;
    }else if (value == 4){
        AL_BZ = pin;
    }else if (value == 5){
        SPres = pin;
    }else if (value == 6){
        SFum = pin;
    }else if (value == 7){
        SJan = pin;
    }else if (value == 8){
        SPor = pin;
    }else if (value == 9){
        SC_IN = pin;
    }else if (value == 10){
        SC_OUT = pin;
    }
    printf("Sensor 1= %d\n", L_01);
    printf("Sensor 2= %d\n", L_02);
    printf("Sensor 3= %d\n", PR);
    printf("Sensor 4= %d\n", AC);
    printf("Sensor 5= %d\n", AL_BZ);
    printf("Sensor 6= %d\n", SPres);
    printf("Sensor 7= %d\n", SFum);
    printf("Sensor 8= %d\n", SJan);
    printf("Sensor 9= %d\n", SPor);
    printf("Sensor 10= %d\n", SC_IN);
    printf("Sensor 11= %d\n", SC_OUT);
}

void pinsSetup(){
    // Define pinos de entrada
    bcm2835_gpio_fsel(SPres, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SFum, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SJan, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SPor, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SC_IN, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SC_OUT, BCM2835_GPIO_FSEL_INPT);
    // Configura pinos de entrada como Pull-down
    bcm2835_gpio_set_pud(SPres, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SFum, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SJan, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SPor, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SC_IN, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SC_OUT, BCM2835_GPIO_PUD_DOWN);

    // Configura pinos das saídas
    bcm2835_gpio_fsel(L_01, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(L_02, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(AC, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(PR, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(AL_BZ, BCM2835_GPIO_FSEL_OUTP);
}

//===================================================================================================================
//                                        Controle das lampadas
//===================================================================================================================
void apagarLampadas(int numLamp){
    sem_wait(&semLampada);
	if (numLamp ==1){
		bcm2835_gpio_write(L_01, LOW );
		fLam1 = 0;
	}else if (numLamp ==2){
    		bcm2835_gpio_write(L_02, LOW );
		fLam2 = 0;
	}else if (numLamp ==3){
		fLam1 = 0;
		fLam2 = 0;
		bcm2835_gpio_write(L_01, LOW);
		bcm2835_gpio_write(L_02, LOW);
	}
    sem_post(&semLampada);
}

void acenderLampada(int numLamp) {
    sem_wait(&semLampada);
    if (numLamp ==1){
        bcm2835_gpio_write(L_01, HIGH );
        fLam1 = 1;
    }else if (numLamp ==2){
        fLam2 = 1;
        bcm2835_gpio_write(L_02, HIGH );
    }else if (numLamp ==3){
        fLam1 = 1;
        fLam2 = 1;
        bcm2835_gpio_write(L_01, HIGH);
        bcm2835_gpio_write(L_02, HIGH);
    }
    sem_post(&semLampada);
}

//===================================================================================================================
//                                        Controle do Projetor
//===================================================================================================================
void ligarProjetor(){
    bcm2835_gpio_write(PR, HIGH );
    fProjetor = 1;
}
void desligarProjetor(){
    bcm2835_gpio_write(PR, LOW );
    fProjetor = 0;
}

//===================================================================================================================
//                                        Controle do Ar-Condicionado
//===================================================================================================================
void ligarAr(){
    bcm2835_gpio_write(AC, HIGH );
    fAr = 1;
}
void desligarAr(){
    bcm2835_gpio_write(AC, LOW );
    fAr = 0;
}

//===================================================================================================================
//                                        Controle do Alarme
//===================================================================================================================
void ligarAlarme(){
    bcm2835_gpio_write(AL_BZ, HIGH );
    fBz = 1;
}
void desligarAlarme(){
    bcm2835_gpio_write(AL_BZ, LOW );
    fSFum = 0;
    fBz = 0;
}

//===================================================================================================================
//                                        Controle do Sistema do alarme
//===================================================================================================================
int ligarSistemaAlarme(){
    int check=0;

    check = checkarSensoresAlarme();
    if (check == 1){
           printf("Sistema de alarme ligado mesmo\n\n");
        fAlarme = 1;
        return 1;
    }else if(check == -2){
    //        printf("Sensor da Janela ativado, desative antes de ligar o sistema de alarme.\n");
        return 0;
    }else if(check == -1){
    //        printf("Sensor de Presença ativado, desative antes de ligar o sistema alarme.\n");
        return 0;
    }else{
    //        printf("Falha ao checkar o alarme.\n");
        return 0;
    }
}

int checkarSensoresAlarme(){
    if(!bcm2835_gpio_lev(SPres)){
        if(!bcm2835_gpio_lev(SJan)){
            return 1;
        }else{
            return -2;
        }
    }else{
        return -1;
    }
}
 
//===================================================================================================================
//                                        Encerramento
//===================================================================================================================
void encerrarPrograma() {
    apagarLampadas(3);
    desligarProjetor();
    desligarAr();
    desligarAlarme();
    pthread_join(sensorsId, NULL);
    bcm2835_close();
    printf("Encerrou com sucesso!!!\n");
    exit(0);
}

void* sensoriamento(void *vargs){
    int contIN=0, contOUT=0,contTemp = 0, auxIN=0,auxOUT=0, tempLampada=0;
    while (1)
    {
        delay(50);
        contTemp++;
        tempLampada++;
        if(bcm2835_gpio_lev(SC_IN)){
            contIN++;
        }else if(bcm2835_gpio_lev(SC_OUT)){
            contOUT++;
        }else{
            if(contIN >= 2 && contIN <=6){
                auxIN = 1;
            }
            if(auxIN == 1){
                qntPessoas++;
            }
            contIN=0;
            auxIN=0;
            if(contOUT >= 2 && contOUT <=6){
                auxOUT = 1;
            }
            if(auxOUT == 1){
                qntPessoas--;
                auxOUT = 0;
            }
            contOUT=0;
            auxOUT=0;
        }
        if(bcm2835_gpio_lev(SPres)){
            acenderLampada(3);
            tempLampada=0;
        }else{
            if(tempLampada >= 300){
                apagarLampadas(3);
                tempLampada=0;
            }
        }
        if (fAlarme){
            if(bcm2835_gpio_lev(SJan)||bcm2835_gpio_lev(SPres)){
                //printf("Buzzer ativado\n");
                ligarAlarme();
            }
        }
        if (bcm2835_gpio_lev(SFum)){
            ligarAlarme();
        }

        if (contTemp%20==0){
            printf("Existem %d pessoas na sala\n", qntPessoas);
            printf("Sitema de alarme em %d\n", fAlarme);
        }
        if (contTemp >= 40){
           // funcDHT22();
            contTemp =0;
        }
        
    }

}

void funcDHT22(){

    int bits[250], data[100];
    int bitidx = 0;
    int counter = 0;
    int laststate = HIGH;
    int j=0;

    // Set GPIO pin to output
    bcm2835_gpio_fsel(DHT22, BCM2835_GPIO_FSEL_OUTP);

    bcm2835_gpio_write(DHT22, HIGH);
    usleep(500000);  // 500 ms
    bcm2835_gpio_write(DHT22, LOW);
    usleep(20000);

    bcm2835_gpio_fsel(DHT22, BCM2835_GPIO_FSEL_INPT);

    data[0] = data[1] = data[2] = data[3] = data[4] = 0;

    // wait for pin to drop?
    while (bcm2835_gpio_lev(DHT22) == 1) {
        usleep(1);
    }

    // read data!
    for (int i=0; i< MAXTIMINGS; i++) {
        counter = 0;
        while ( bcm2835_gpio_lev(DHT22) == laststate) {
        counter++;
        //nanosleep(1);		// overclocking might change this?
            if (counter == 1000)
        break;
        }
        laststate = bcm2835_gpio_lev(DHT22);
        if (counter == 1000) break;
        bits[bitidx++] = counter;

        if ((i>3) && (i%2 == 0)) {
        // shove each bit into the storage bytes
        data[j/8] <<= 1;
        if (counter > 200)
            data[j/8] |= 1;
        j++;
        }
    }

    printf("Data (%d): 0x%x 0x%x 0x%x 0x%x 0x%x\n", j, data[0], data[1], data[2], data[3], data[4]);

    if ((j >= 39) && (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)) ) {
        
        float f, h;
        h = data[0] * 256 + data[1];
        h /= 10;

        f = (data[2] & 0x7F)* 256 + data[3];
            f /= 10.0;
        if (data[2] & 0x80)  f *= -1;
            printf("Temp =  %.1f *C, Hum = %.1f \%\n", f, h);

    }
}

