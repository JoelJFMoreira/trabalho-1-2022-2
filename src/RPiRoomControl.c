#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <bcm2835.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <semaphore.h>

//Configuração pinos
//Saidas
#define L_01 RPI_V2_GPIO_P1_37     // BCM 26
#define L_02 RPI_V2_GPIO_P1_35     // BCM 19
#define AC RPI_V2_GPIO_P1_33       // BCM 13
#define PR RPI_V2_GPIO_P1_31       // BCM 06
#define AL_BZ RPI_V2_GPIO_P1_29    // BCM 05

//Entradas
#define SPres 0                    // BCM 11 | gpio0
#define SFum RPI_V2_GPIO_P1_23     // BCM 11
#define SJan RPI_V2_GPIO_P1_21     // BCM 09
#define SPor RPI_V2_GPIO_P1_19     // BCM 10
#define SC_IN RPI_V2_GPIO_P1_15    // BCM 22
#define SC_OUT RPI_V2_GPIO_P1_13   // BCM 27
#define DHT22 RPI_V2_GPIO_P1_12    // BCM 18

//Porta do sockets 
#define PORT 10833                 //10831 10840

//flags 
int fSFum = 0;
int fLampada = 0;
int fProjetor = 0;
int fAr = 0;
int fAlarme = 0;
int fBz = 0;
int client_fd = 0;

sem_t semLampada;

// Inicializacao
void pinsSetup();

int socketSetup();
// Controle das lampadas
void apagarLampadas();
void acenderLampada();

// Controle Projetor
void ligarProjetor();
void desligarProjetor();

// Controle Ar
void ligarAr();
void desligarAr();

// Controle Alarme
void ligarAlarme();
void desligarAlarme();

// Sistema do Alarme
int ligarSistemaAlarme();
int checkarSensoresAlarme();

void encerrarPrograma();

// Contagem de pessoas na sala
int quantidadePessoasSala();

pthread_t sensorsId;

// Sensoriamento
void* sensoriamento(void *vargs);


int main(int argc, char **argv){   
    int sock =0;
    int valread;
    int alarmError;
    char buffer[1024] = { 0 };
    char msg[5];

    sem_init(&semLampada,0,1);
    pthread_create(&sensorsId, NULL, &sensoriamento, NULL);

    if (!bcm2835_init())
        exit(1);

    pinsSetup();
    sock = socketSetup();
    signal(SIGINT, encerrarPrograma);

    int counter;

    while (1){

        bzero(buffer, 1024);
        valread = read(sock, buffer, 1024);
        
        if(strncmp(buffer, "exit", 5) == 0){
            encerrarPrograma();
            break;
        }

        else{
            if(strncmp(buffer,"!lamp", 5) == 0){
                if(fLampada==1){
                    apagarLampadas();
                    printf("Lampadas apagadas\n");
                }
                else{
                    acenderLampada();
                    printf("Lampadas ligadas\n");
                }
            }else if(strncmp(buffer,"!proj", 5) == 0){
                if(fProjetor==1){
                    desligarProjetor();
                    printf("Projetor desligado\n");
                }else{
                    ligarProjetor();
                    printf("Projetor ligado\n");
                }
            }else if(strncmp(buffer,"!arcn", 5) == 0){
                if(fAr==1){
                    desligarAr();
                    printf("Ar-Condicionado desligado\n");
                } 
                else{
                    ligarAr();
                    printf("Ar-Condicionado ligado\n");
                }
            }else if(strncmp(buffer,"!alar", 5) == 0){
                if(fAlarme==1){
                    desligarAlarme();
                    fAlarme=0;
                    printf("Sistema de alarme desligado\n");
                }else{
                    alarmError = checkarSensoresAlarme();
                    if (alarmError==1){
                        printf("Sistema de alarme ligado\n");
                        ligarSistemaAlarme();
                    }else if (alarmError == -2){
                        printf("Sensor da Janela ativado, desative antes de ligar o sistema de alarme.\n");
                    }else if(check == -1){
                       printf("Sensor de Presença ativado, desative antes de ligar o sistema alarme.\n");
                    }
                }
            }else if(strncmp(buffer,"!stat", 5) == 0){
                int statusConcatenado=0;
                statusConcatenado = (fLampada & fSFum & fProjetor & fAr & fAlarme & fBz );
                printf ("Status = ")
            }else{
                //printf("%s\n", buffer);
            }
            
            // strcpy(buffer,"LampadasLigada");
            // send(sock, buffer, strlen(buffer), 0); 
        }
    }

    // closing the connected socket
    close(client_fd);
}

int socketSetup(){
    int sock = 0;
    struct sockaddr_in serv_addr;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
 
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
 
    // Convert IPv4 and IPv6 addresses from text to binary
    // form
    if (inet_pton(AF_INET, "192.168.1.129", &serv_addr.sin_addr)<= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    client_fd = connect(sock, (struct sockaddr*)&serv_addr,sizeof(serv_addr));

    if (client_fd < 0){
        perror("Connection Failed");
        return -1;
    }

    return sock;
}

void pinsSetup(){

    // Define botão como entrada
    bcm2835_gpio_fsel(SPres, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SFum, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SJan, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SPor, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SC_IN, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SC_OUT, BCM2835_GPIO_FSEL_INPT);
    // Configura entrada do botão como Pull-down
    bcm2835_gpio_set_pud(SPres, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SFum, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SJan, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SPor, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SC_IN, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SC_OUT, BCM2835_GPIO_PUD_DOWN);

    // Configura pinos dos LEDs como saídas
    bcm2835_gpio_fsel(L_01, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(L_02, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(AC, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(PR, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(AL_BZ, BCM2835_GPIO_FSEL_OUTP);
}

//===================================================================================================================
//                                        Controle das lampadas
//===================================================================================================================
void apagarLampadas(){
    sem_wait(&semLampada);
    bcm2835_gpio_write(L_01, LOW );
    bcm2835_gpio_write(L_02, LOW );
    fLampada = 0;
    sem_post(&semLampada);
}

void acenderLampada(int qntPessoas) {
    sem_wait(&semLampada);
    bcm2835_gpio_write(L_01, HIGH );
    bcm2835_gpio_write(L_02, HIGH );
    fLampada = 1;
    sem_post(&semLampada);
}

//===================================================================================================================
//                                        Controle do Projetor
//===================================================================================================================
void ligarProjetor(){
    bcm2835_gpio_write(PR, HIGH );
    fProjetor = 1;
}
void desligarProjetor(){
    bcm2835_gpio_write(PR, LOW );
    fProjetor = 0;
}

//===================================================================================================================
//                                        Controle do Ar-Condicionado
//===================================================================================================================
void ligarAr(){
    bcm2835_gpio_write(AC, HIGH );
    fAr = 1;
}
void desligarAr(){
    bcm2835_gpio_write(AC, LOW );
    fAr = 0;
}

//===================================================================================================================
//                                        Controle do Alarme
//===================================================================================================================
void ligarAlarme(){
    bcm2835_gpio_write(AL_BZ, HIGH );
    fBz = 1;
}
void desligarAlarme(){
    bcm2835_gpio_write(AL_BZ, LOW );
    fSFum = 0;
    fBz = 0;
}


void encerrarPrograma() {
    apagarLampadas();
    desligarProjetor();
    desligarAr();
    desligarAlarme();
    pthread_join(sensorsId, NULL);
    bcm2835_close();
    printf("Encerrou com sucesso!!!\n");
    exit(0);
}

void* sensoriamento(void *vargs){
    int contIN=0, contOUT=0,contTemp = 0, qntPessoas = 0, auxIN=0,auxOUT=0, tempLampada=0;
    while (1)
    {
        delay(50);
        contTemp++;
        tempLampada++;
        if(bcm2835_gpio_lev(SC_IN)){
            contIN++;
        }else if(bcm2835_gpio_lev(SC_OUT)){
            contOUT++;
        }else{
            if(contIN >= 2 && contIN <=6){
                auxIN = 1;
            }
            if(auxIN == 1){
                qntPessoas++;
            }
            contIN=0;
            auxIN=0;
            if(contOUT >= 2 && contOUT <=6){
                auxOUT = 1;
            }
            if(auxOUT == 1){
                qntPessoas--;
                auxOUT = 0;
            }
            contOUT=0;
            auxOUT=0;
        }
        if(qntPessoas >= 1){
            acenderLampada(qntPessoas);
            tempLampada=0;
        }else{
            if(tempLampada >= 300){
                apagarLampadas();
                tempLampada=0;
            }
        }
        if (fAlarme){
            if(bcm2835_gpio_lev(SJan)||bcm2835_gpio_lev(SPres)){
                //printf("Buzzer ativado\n");
                ligarAlarme();
                fBz = 1;
            }
        }
        if (bcm2835_gpio_lev(SFum)){
            fSFum = 1;
            ligarAlarme();
        }

        if (contTemp%20==0){
            
            printf("Existem %d pessoas na sala\n", qntPessoas);
            printf("Sitema de alarme em %d\n", fAlarme);
        }
        if (contTemp >= 40){
           // funcDHT22();
            contTemp =0;
        }
        
    }

}


int ligarSistemaAlarme(){
    int check=0;
    check = checkarSensoresAlarme();
    if (check == 1){
           printf("Sistema de alarme ligado mesmo\n\n");
        fAlarme = 1;
        return 1;
    }else if(check == -2){
    //        printf("Sensor da Janela ativado, desative antes de ligar o sistema de alarme.\n");
        return 0;
    }else if(check == -1){
    //        printf("Sensor de Presença ativado, desative antes de ligar o sistema alarme.\n");
        return 0;
    }else{
    //        printf("Falha ao checkar o alarme.\n");
        return 0;
    }
}

int checkarSensoresAlarme(){
    if(!bcm2835_gpio_lev(SPres)){
        if(!bcm2835_gpio_lev(SJan)){
            return 1;
        }else{
            return -2;
        }
    }else{
        return -1;
    }
}
 
